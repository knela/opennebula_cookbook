default[:opennebula_centos][:bridge][:name] = "br0"
default[:opennebula_centos][:bridge][:onboot] = "yes"
default[:opennebula_centos][:bridge][:ip] = "172.16.100.1"
default[:opennebula_centos][:bridge][:netmask] = "255.255.255.0"

if node['config']['env'] == 'local'
  default[:opennebula_centos][:sunstone][:pass] = "set-local-pass"
elsif node['config']['env'] == 'dev'
  default[:opennebula_centos][:sunstone][:pass] = "set-dev-pass"
elsif node['config']['env'] == 'prod'
  default[:opennebula_centos][:sunstone][:pass] = "set-prod-pass"
else
  default[:opennebula_centos][:sunstone][:pass] = "set-default-pass"
end
