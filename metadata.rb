#name             'opennebula'
name             'opennebula_centos'
maintainer       'knelasevero'
maintainer_email 'lucas.alves@uplug.com.br'
license          'MIT'
description      'Installs/Configures opennebula'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
