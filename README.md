# Opennebula Cookbook

Installs and configures Opennebula on a Centos machine


## Requirements

You need a Centos machine to run this cookbook against. Tried to make this cookbook as clean as possible, not depending on other cookbooks.

### Platforms

- CentOS

### Chef

- Chef 12.0 or later

### Cookbooks

None.

## Usage

### opennebula_centos::default

Just include `opennebula_centos::default` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[opennebula_centos]"
  ]
}
```

## License

- MIT

