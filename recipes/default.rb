execute 'disable selinux if permissive' do
  command "grep -q 'SELINUX=permissive' /etc/sysconfig/selinux && sed -i 's/SELINUX=permissive*/SELINUX=disabled/' /etc/sysconfig/selinux"
  command "setenforce 0"
  only_if "grep -q 'SELINUX=permissive' /etc/sysconfig/selinux"
end

execute 'disable selinux if enforcing' do
  command "grep -q 'SELINUX=enforcing' /etc/sysconfig/selinux && sed -i 's/SELINUX=enforcing*/SELINUX=disabled/' /etc/sysconfig/selinux"
  command "setenforce 0"
  only_if "grep -q 'SELINUX=enforcing' /etc/sysconfig/selinux"
end


package 'epel-release'

cookbook_file '/etc/yum.repos.d/opennebula.repo' do
  source 'opennebula.repo'
  owner 'root'
  group 'root'
  mode '644'
  action :create
end
package 'opennebula-server'
package 'opennebula-sunstone'
package 'opennebula-ruby'
package 'opennebula-gate'
package 'opennebula-flow'
package 'redhat-lsb'

execute 'yum without confirmation' do
  command "grep -q 'assumeyes=0' /etc/yum.conf && sed -i 's/assumeyes=0*/assumeyes=1/' /etc/yum.conf || echo 'assumeyes=1' >> /etc/yum.conf"
  not_if "grep -q 'assumeyes=1' /etc/yum.conf"
end

execute 'install dependencies' do
  command 'echo -e "\n" "\n"| /usr/share/one/install_gems'
end

execute 'set password' do
  command "echo 'oneadmin:#{node.opennebula_centos.sunstone.pass}' > /var/lib/one/.one/one_auth"
  user 'oneadmin'
  group 'oneadmin'
end

execute 'run opennebula on all interfaces' do
  command "sed -i 's/host: 127.0.0.1/host: 0.0.0.0/' /etc/one/sunstone-server.conf"
  only_if "grep -q 'host: 127.0.0.1' /etc/one/sunstone-server.conf"
end

service 'opennebula' do
  supports [:stop, :start, :restart]
  action [:start, :enable]
end

service 'opennebula-sunstone' do
  supports [:stop, :start, :restart]
  action [:start, :enable]
end

cookbook_file 'var/lib/one/.ssh/config' do
  source 'ssh_one'
  owner 'oneadmin'
  group 'oneadmin'
  mode '600'
  action :create
end

package 'opennebula-node-kvm'

service 'libvirtd' do
  supports [:stop, :start, :restart]
  action [:start, :enable]
end

template '/etc/sysconfig/network-scripts/ifcfg-br0' do
  owner 'root'
  group 'root'
  mode '644'
  variables({
    bridge: node.opennebula_centos.bridge.name,
    onboot: node.opennebula_centos.bridge.onboot,
    ip: node.opennebula_centos.bridge.ip,
    netmask: node.opennebula_centos.bridge.netmask
  })  
end

execute 'restart bridge interface' do
  command "bash -c 'ifdown #{node.opennebula_centos.bridge.name} && ifup #{node.opennebula_centos.bridge.name}'"
end

execute 'register a worker node' do
  command 'onehost create localhost -i kvm -v kvm'
  not_if 'onehost list | grep -q list || onehost list | grep -q on'
end

cookbook_file '/var/lib/one/mynetwork.one' do
  source 'mynetwork.one'
  owner 'oneadmin'
  group 'oneadmin'
  mode '644'
  action :create
end

execute 'create virtual network' do
  command 'onevnet create /var/lib/one/mynetwork.one'
  user 'oneadmin'
  not_if 'onevnet list | grep -q private'
end
